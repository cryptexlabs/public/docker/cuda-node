FROM nvidia/cuda:11.1-devel-ubuntu18.04 as build

# Disable spam
ENV SUPPRESS_SUPPORT=true
ENV OPENCOLLECTIVE_HIDE=true
ENV DISABLE_OPENCOLLECTIVE=true
ENV NPM_CONFIG_FUND=false
ENV CUDA_LIB_PATH=/usr/local/cuda/lib64

# Env
ARG NODE_ENVIRONMENT=production
ENV NODE_ENV=$NODE_ENVIRONMENT
ENV HTTP_PORT=80
ENV TIME_ZONE="America/Chicago"
ENV HUSKY_SKIP_INSTALL=1
ENV LD_LIBRARY_PATH=/lib64:/usr/local/cuda/lib64/
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update

# Set the timezone in docker
RUN apt-get -y install tzdata \
   && eval "cp /usr/share/zoneinfo/$TIME_ZONE /etc/localtime" \
   && echo "$TIME_ZONE" > /etc/timezone

# ubuntu dependencies
RUN apt-get -y install \
    bash \
    python3-pip \
    libtool \
    python \
    python3 \
    build-essential \
    make \
    autoconf \
    automake \
    libc6-dev \
    git \
    curl \
    dirmngr \
    apt-transport-https \
    lsb-release \
    ca-certificates

SHELL ["/bin/bash", "-c" ]

# Install nodejs and npm
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt-get update && apt-get -y install nodejs=14.15.1-1nodesource1

# Install yarn
RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install yarn

# Installs latest Chromium package.
RUN curl -LO "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
RUN apt-get install -y ./google-chrome-stable_current_amd64.deb
RUN rm google-chrome-stable_current_amd64.deb